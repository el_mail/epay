import os
from unittest import TestCase
from epay.process import get_context

os.environ["MERCHANT_CERTIFICATE_ID"]="00c183d70b"
os.environ["MERCHANT_NAME"]="Demo Shop 3"
os.environ["PRIVATE_KEY_FN"]="./paysys/cert.prv"
os.environ["PRIVATE_KEY_PASS"]="1q2w3e4r"
os.environ["PUBLIC_KEY_FN"]="./paysys/kkbca.pem"
os.environ["MERCHANT_ID"]="92061103"

class EpayTestCase(TestCase):

    def test_epay(self):
        print(get_context(amount=10000, order_id=1, instalment='yes',inst_period="3;6"))
        self.assertEqual(type(get_context(amount=10000, order_id=1)), str) #TODO: assertEqual base64