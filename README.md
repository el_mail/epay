
Python package для работы с Qazkom ePay
=====


Установка
------------

1.	Установка через pip:
	
		pip install git+ssh://git@gitlab.com/unie.kz/integrations/epay.git

1. Добавьте в `.env`:

		MERCHANT_CERTIFICATE_ID = "" # Серийный номер сертификата Cert Serial Number
		MERCHANT_NAME = "" # Название магазина (продавца) Shop/merchant Name
		PRIVATE_KEY_FN = "" # Абсолютный путь к закрытому ключу Private cert path
		PRIVATE_KEY_PASS = "" # Пароль к закрытому ключу Private cert password
		PUBLIC_KEY_FN = "" # Абсолютный путь к открытому ключу Public cert path
		MERCHANT_ID="" # Терминал ИД в банковской Системе



Использование
-----

Отправка в систему авторизации::
	
	import epay
	context = epay.get_context(order_id = '333',amount="666")
	или
	context = epay.get_context(order_id = '333', amount="666", currency_id = "398") 
	# currency_id  - 840-USD, 398-Tenge

	
Обработка документа возвращаемого системой авторизации::
	
	import epay

	response = request.POST['response']

	result = epay.postlink(response)
	if result.status:
		# операция прошла успешно
		# все данные в result.data 
		# (result.data['ORDER_AMOUNT'],result.data['ORDER_ID'],....)
	else:
		print result.message
	
Данные в result.data::

	result.data
	
		BANK_NAME
		CUSTOMER_NAME
		CUSTOMER_MAIL
		CUSTOMER_PHONE
		MERCHANT_CERT_ID
		MERCHANT_NAME
		ORDER_ID
		ORDER_AMOUNT
		ORDER_CURRENCY
		DEPARTMENT_MERCHANT_ID
		DEPARTMENT_AMOUNT
		MERCHANT_SIGN_TYPE
		CUSTOMER_SIGN_TYPE
		RESULTS_TIMESTAMP
		PAYMENT_MERCHANT_ID
		PAYMENT_AMOUNT
		PAYMENT_REFERENCE
		PAYMENT_APPROVAL_CODE
		PAYMENT_RESPONSE_CODE
		BANK_SIGN_CERT_ID
		BANK_SIGN_TYPE
		LETTER
		SIGN
		RAWSIGN

